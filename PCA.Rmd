---
title: "R Notebook"
output: html_notebook
---

# PCA with FactoMineR

As you saw in the video, FactoMineR is a very useful package, rich in functionality, that implements a number of dimensionality reduction methods. Its function for doing PCA is PCA() - easy to remember! Recall that PCA(), by default, generates 2 graphs and extracts the first 5 PCs. You can use the ncp argument to manually set the number of dimensions to keep.

You can also use the summary() function to get a quick overview of the indices of the first three principal components. Moreover, for extracting summaries of some of the rows in a dataset, you can specify the nbelements argument. You'll have a chance to practice all of these and more in this exercise!

As in the previous lesson, the cars dataset is available in your workspace.

```{r}
install.packages("FactoMineR")
install.packages("tidyverse")
install.packages("factoextra")
install.packages("ade4")
install.packages("paran")
install.packages("psych")
install.packages("missMDA")
install.packages("pcaMethods")
```

```{r}
library(FactoMineR)
library(factoextra)
library(tidyverse)
library(ade4)
library(paran)
library(psych)
library(missMDA)
cars = mtcars
```


```{r}
# Run a PCA for the 10 non-binary numeric variables of cars.
pca_output_ten_v <- PCA(cars, ncp = 4, graph = FALSE)
```


```{r}
# Get the summary of the first 100 cars.
summary(pca_output_ten_v, nbelements = 100)
```


```{r}
# Get the variance of the first 3 new dimensions.
pca_output_ten_v$eig[,2][1:3]
```


```{r}
# Get the cumulative variance.
pca_output_ten_v$eig[,3][1:3]
```

# Exploring PCA()

PCA() provides great flexibility in its usage. You can choose to ignore some of the original variables or individuals in building a PCA model by supplying PCA() with the ind.sup argument for supplementary individuals and quanti.sup or quali.sup for quantitative and qualitative variables respectively. Supplementary individuals and variables are rows and variables of the original data ignored while building the model.

Your learning objectives in this exercise are:

- To conduct PCA considering parts of a dataset
-  To inspect the most correlated variables with a specified principal component
- To find the contribution of variables in the designation of the first 5 principal components

Go for it! The cars dataset is available in your workspace.

```{r}
# Run a PCA with active and supplementary variables
pca_output_all <- PCA(cars, quanti.sup = 1, quali.sup = 2, graph = FALSE)

# Get the most correlated variables
dimdesc(pca_output_all, axes = 1:2)
```


```{r}
# Get the most correlated variables
dimdesc(pca_output_all, axes = 1:2)

```


```{r}

# Run a PCA on the first 100 car categories
pca_output_hundred <- PCA(cars, ind.sup = 101:nrow(cars), graph = FALSE)

```


```{r}

# Trace variable contributions in pca_output_hundred
pca_output_hundred$var$contrib
```

# PCA with ade4

Alright! Now that you've got some real hands-on experience with FactoMineR, let's have a look at ade4, a well-known and well-maintained R package with a large number of numerical methods for building and handling PCA models. dudi.pca() is the main function that implements PCA for ade4 and by default, it is interactive: It lets the user insert the number of retained dimensions. For suppressing the interactive mode and inserting the number of axes within the dudi.pca() function, you need to set the scannf argument to FALSE and then use the nf argument for setting the number of axes to retain. So, let's put ade4 into practice and compare it with FactoMineR.
Instructions
100 XP

- Run PCA on the numeric variables of cars with the ade4 package and, in a non-interactive mode, extract the first 4 principal components.
- In the next two lines of code, you are asked to explore with summary() the resulting object cars_pca and pca_output_ten_v, the PCA model object that you built with FactoMineR earlier that is available in your workspace. Spend a few minutes in comparing the difference in the output of the two summaries.

```{r}
# Run a PCA using the 10 non-binary numeric variables.
cars_pca <- dudi.pca(cars, scannf = FALSE, nf = 4)

# Explore the summary of cars_pca.
summary(cars_pca)

# Explore the summary of pca_output_ten_v.
summary(pca_output_ten_v)
```

# Plotting cos2

You're getting the hang of PCA now! As Alex demonstrated in the video, an important index included in your PCA models is the squared cosine, abbreviated in FactoMineR and factoextra as cos2. This shows how accurate the representation of your variables or individuals on the PC plane is.

The factoextra package is excellent at handling PCA models built using FactoMineR. Here, you're going to explore the functionality of factoextra. You'll be using the pca_output_all object that you computed earlier and create plots based on its cos2. Visual aids are key to understanding cos2.

```{r}
# Create a factor map for the variables.
fviz_pca_var(pca_output_all, select.var = list(cos2 = 0.7), repel = TRUE)
```


```{r}
# Modify the code to create a factor map for the individuals.
fviz_pca_ind(pca_output_all, select.ind = list(cos2 = 0.7), repel = TRUE)
```
Create a barplot for the 10 variables with the highest cos2 on the 1st PC. Use "var" for variables, instead of "ind".

```{r}
# Create a barplot for the variables with the highest cos2 in the 1st PC.
fviz_cos2(pca_output_all, choice = "var", axes = 1, top = 10)
```

# Plotting contributions

In this exercise, you will be asked to prepare a number of plots to help you get a better feeling of the variables' contributions on the extracted principal components. It is important to keep in mind that the contributions of the variables essentially signify their importance for the construction of a given principal component.

For this, you need to adjust the select.var argument of the fviz_pca_var() function. As in the previous exercise, you also need to add the argument repel argument for rendering the output more readable. Recall that in the previous exercise, you used the list() function with the cos2 argument. This time, instead of cos2, you need to use the contrib character value.

```{r}
# Create a factor map for the top 5 variables with the highest contributions.
fviz_pca_var(pca_output_all, select.var = list(contrib = 5), repel = TRUE)

```


```{r}


# Create a factor map for the top 5 individuals with the highest contributions.
fviz_pca_ind(pca_output_all, select.ind = list(contrib = 5), repel = TRUE)
```


```{r}
# Create a barplot for the variables with the highest contributions to the 1st PC.
fviz_contrib(pca_output_all, choice = "var", axes = 1, top = 5)

```

# Biplots and their ellipsoids

As mentioned in the video, biplots are graphs that provide a compact way of summarizing the relationships between individuals, variables, and also between variables and individuals within the same plot. Moreover, ellipsoids can be added on top of a biplot and offer a much better overview of the biplot based on the groupings of variables and individuals.

In this exercise, your job is to create biplots and ellipsoids using factoextra's graphical utilities.

```{r}
fviz_pca_biplot(pca_output_all)
```

Ellipsoids can be added within the individuals factors' map to group individuals that resemble each other in the coordinate system of the principal components. Create ellipsoids based on the levels of the supplementary variable wheeltype.
```{r}
fviz_pca_ind(pca_output_all, habillage = as.factor(cars$cyl), addEllipses = TRUE)
```


```{r}
fviz_pca_biplot(pca_output_all, habillage = as.factor(cars$cyl), addEllipses = TRUE,alpha.var="cos2")
```

# Determining number of PCs to keep


## The Kaiser-Guttman rule and the Scree test

In the video, you saw the three most common methods that people utilize to decide the number of principal components to retain:

- Kaiser-Guttman rule
- Scree test (constructing the screeplot)
- Parallel Analysis

Your task now is to apply all of them on the R's built-in airquality dataset!

```{r}
# Conduct a PCA on the airquality dataset
pca_air <- PCA(airquality, graph = FALSE)

# Perform the screeplot test
fviz_screeplot(pca_air, ncp = 5)
```


```{r}
# Apply the Kaiser-Guttman rule
summary(pca_air, ncp = 4)
```

Parallel Analysis with paran()

In this exercise, you will use two R functions for conducting parallel analysis for PCA:

- paran() of the paran package and
- fa.parallel() of the psych package.

fa.parallel() has one advantage over the paran() function; it allows you to use more of your data while building the correlation matrix. On the other hand, paran() does not handle missing data and you should first exclude missing values before passing the data to the function. For checking out the suggested number of PCs to retain, fa.parallel()'s output object includes the attribute ncomp.

The built-in R dataset airquality, on which you will be doing your parallel analyses, describes daily air quality measurements in New York from May to September 1973 and includes missing values.

```{r}
# Subset the complete rows of airquality.
airquality_complete <- airquality[complete.cases(airquality), ]

# Conduct a parallel analysis with paran().
air_paran <- paran(airquality_complete, seed = 1)

# Check out air_paran's suggested number of PCs to retain.
air_paran$Retained

# Conduct a parallel analysis with fa.parallel().
air_fa_parallel <- fa.parallel(airquality)

# Check out air_fa_parallel's suggested number of PCs to retain.
air_fa_parallel$ncomp
```

# Handling NAs in PCA

Estimating missing values with missMDA

As you saw in the video, In R, there are two packages for conducting PCA to a dataset with missing values; pcaMethods and missMDA. In this exercise, you are going to use the first method introduced in the video: combining missMDA and FactoMineR. Both packages are loaded for you in this exercise.

The two-step procedure includes a) the estimation of missing values by using an iterative PCA algorithm in the first place and b) number of dimensions for PCA by cross-validation.

In this exercise, you will be working with the ozone dataset of the missMDA package that includes 112 daily measurements of meteorological variables (wind speed, temperature, rainfall, etc.) and ozone concentration recorded in Rennes (France) during the summer 2001.


- Find the number of cells with missing values in the first 11 continuous variables of the ozone dataset.
- Estimate the number of dimensions used for imputing the data.
The last step is to use the estimated number stored in ozone_ncp in order to actually conduct the data imputation on ozone[,1:11].

- Use the sum() and is.na() functions to achieve that.
- Recall that the right function to apply on ozone[,1:11] is the estim_ncpPCA() function.
- You need to used the imputePCA() function and complete its ncp argument with the ncp attribute of ozone_ncp.


```{r}
# Check out the number of cells with missing values.
sum(is.na(ozone[,1:11]))

```


```{r}

# Estimate the optimal number of dimensions for imputation.
ozone_ncp <- estim_ncpPCA(ozone[,1:11]) 

```


```{r}

# Do the actual data imputation. 
complete_ozone <- imputePCA(ozone[,1:11], ncp = ozone_ncp$ncp, scale = TRUE)
```


```{r}
```


```{r}
```


```{r}
```


```{r}
```


```{r}
```


```{r}
```


```{r}
```


```{r}
```


```{r}
```

# References

- https://personal.utdallas.edu/~herve/abdi-awPCA2010.pdf